//
//  RDPDFPage.h
//
//  Created by Roman Kolesnichenko on 05.03.15.
//  Copyright (c) 2015 Roman Kolesnichenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RDPDFConstants.h"
#import "RDPDFElement.h"

@interface RDPDFPage : NSObject

-(instancetype)initWithSize:(CGSize)size;
+(instancetype)pageWithSize:(CGSize)size;
+(instancetype)pageWithNibNamed:(NSString*)nibName values:(NSDictionary*)values;

-(void) addElement:(RDPDFElement*)element;
-(void)insertElement:(RDPDFElement *)element atIndex:(NSUInteger)index;
-(NSUInteger)indexOfElement:(RDPDFElement *)element;

@property (nonatomic, assign) CGSize size;
@property (nonatomic, strong) NSMutableArray *elements;

@end
