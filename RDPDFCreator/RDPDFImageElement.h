//
//  RDPDFImageElement.h
//
//  Created by Roman Kolesnichenko on 05.03.15.
//  Copyright (c) 2015 Roman Kolesnichenko. All rights reserved.
//

#import "RDPDFElement.h"

@interface RDPDFImageElement : RDPDFElement

+(RDPDFImageElement *)imageNamed:(NSString *)name;
+(RDPDFImageElement *)image:(UIImage *)image;

-(instancetype)initWithImageName:(NSString*)imageName;
-(instancetype)initWithImage:(UIImage*)image;

-(void) render;

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *imageName;

@end
