//
//  RDPDFCreator.h
//
//  Created by Roman Kolesnichenko on 05.03.15.
//  Copyright (c) 2015 Roman Kolesnichenko. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "RDPDFConstants.h"

#import "RDPDFPage.h"
#import "RDPDFElement.h"
#import "RDPDFImageElement.h"
#import "RDPDFTextElement.h"


@interface RDPDFCreator : NSObject

-(instancetype)initWithFileName:(NSString*)fileName;

/**
 Generate PDF file
 @return If succeeded - return full path to PDF file else return nil.
 */
-(NSString*)render;

/**
 Add page
 @param page 
 The RDPDFPage object to add to the pages array. This value must not be nil.

 */
-(void)addPage:(RDPDFPage*)page;

/**
 Insert page at index

 @param page
 The RDPDFPage object to add to the pages array. This value must not be nil.
 
 @param index
 The index in the pages array at which to insert page. This value must positive and not be greater than the count of pages.
 
 */
-(void)insertPage:(RDPDFPage *)page atIndex:(NSUInteger)index;

/**
 Return RDPDFPage object index. If none of the object in the pages array is equal to page object, returns NSNotFound.

 @param page
 The RDPDFPage object to add to the pages array. This value must not be nil.

 @return index
 */
-(NSInteger)indexOfPage:(RDPDFPage *)page;

/**
 Array of pages. Is readonly property - for add or insert page use addPage: or insertPage:atIndex methods.
 */
@property (nonatomic, readonly) NSMutableArray *pages;

/**
 Name of the PDF file. Default value is "dummy.pdf"
 */
@property (nonatomic, strong) NSString *fileName;

/**
 The full path to a directory where will be generated the PDF file
 */
@property (nonatomic, strong) NSString *filePath;

@end
