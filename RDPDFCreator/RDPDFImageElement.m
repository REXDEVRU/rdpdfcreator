//
//  RDPDFImageElement.m
//
//  Created by Roman Kolesnichenko on 05.03.15.
//  Copyright (c) 2015 Roman Kolesnichenko. All rights reserved.
//

#import "RDPDFImageElement.h"

@implementation RDPDFImageElement

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (instancetype)initWithImageName:(NSString*)imageName
{
    self = [super init];
    if (self) {
        self.imageName = imageName;
    }
    return self;
}

- (instancetype)initWithImage:(UIImage*)image
{
    self = [super init];
    if (self) {
        self.image = image;
    }
    return self;
}

#pragma mark -
+(RDPDFImageElement *)imageNamed:(NSString *)imageName
{
    return [[RDPDFImageElement alloc] initWithImageName:imageName];
}

+(RDPDFImageElement *)image:(UIImage *)image
{
    return [[RDPDFImageElement alloc] initWithImage:image];
}

#pragma mark -
-(void)setImageName:(NSString *)imageName
{
    _imageName = imageName;
    self.image = [UIImage imageNamed:imageName];
}

-(void)setImage:(UIImage *)image
{
   
    _image = image;
    self.size = image.size;
}

-(void)render
{
    if (self.image)
    {
        [self.image drawInRect:self.frame];
    }
}

@end
