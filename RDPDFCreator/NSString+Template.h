//
//  NSString+Template.h
//
//  Created by Roman Kolesnichenko on 05.03.15.
//  Copyright (c) 2015 Roman Kolesnichenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Template)

-(BOOL)isTemplate;
-(BOOL)isTemplateWithParam;

-(NSNumber*)paramValue;
-(NSString*)templateName;
-(NSArray*)splitByLength:(NSUInteger)len;
-(NSArray*)splitByOneSymbol;
@end
