//
//  RDPDFElement.h
//
//  Created by Roman Kolesnichenko on 05.03.15.
//  Copyright (c) 2015 Roman Kolesnichenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RDPDFElement : NSObject

-(void)render;

@property (nonatomic, assign) CGRect frame;
@property (nonatomic, assign) CGPoint position;

@property (nonatomic, assign) CGSize size;

@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;

@property (nonatomic, assign) BOOL visible;

@end
