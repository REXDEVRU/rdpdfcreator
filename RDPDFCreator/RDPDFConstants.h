//
//  RDPDFConstants.h
//
//  Created by Roman Kolesnichenko on 05.03.15.
//  Copyright (c) 2015 Roman Kolesnichenko. All rights reserved.
//

#define DEFAULT_FILE_NAME @"dummy.pdf"
#define DEFAULT_PAGE_SIZE PAGE_SIZE_A4

#define PAGE_SIZE_A4 (CGSize){595, 842}
#define PAGE_SIZE_LETTER (CGSize){612, 792}

#define DEFAULT_TEXT_COLOR [UIColor blackColor]
#define DEFAULT_TEXT_FONT [UIFont systemFontOfSize:14]
