//
//  RDPDFCreator.m
//
//  Created by Roman Kolesnichenko on 05.03.15.
//  Copyright (c) 2015 Roman Kolesnichenko. All rights reserved.
//

#import "RDPDFCreator.h"

@interface RDPDFCreator()

@property (nonatomic, strong) NSMutableArray *pages;

@end

@implementation RDPDFCreator

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        self.fileName = DEFAULT_FILE_NAME;
    }
    return self;
}

-(instancetype)initWithFileName:(NSString*)fileName
{
    self = [super init];
    if (self)
    {
        self.fileName = fileName;
    }
    return self;
}

#pragma mark -
-(void)setFileName:(NSString *)fileName
{
    _fileName = [fileName lastPathComponent];
    
    NSString *filePath = [fileName stringByDeletingLastPathComponent];
    if (filePath.length > 0)
    {
        self.filePath = filePath;
    }
}

-(NSString *)filePath
{
    if (!_filePath)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        _filePath = [paths objectAtIndex:0];
    }
    
    return _filePath;
}

#pragma mark -
-(void)addPage:(RDPDFPage *)page
{
    if ([page isKindOfClass:[RDPDFPage class]])
    {
        [self.pages addObject:page];
    }
}

-(void)insertPage:(RDPDFPage *)page atIndex:(NSUInteger)index
{
    if ([page isKindOfClass:[RDPDFPage class]])
    {
        [self.pages insertObject:page atIndex:index];
    }
}

-(NSInteger)indexOfPage:(RDPDFPage *)page
{
    return [self.pages indexOfObject:page];
}

#pragma mark -
-(NSMutableArray *)pages
{
    if (!_pages)
    {
        _pages = [NSMutableArray new];
    }
    return _pages;
}

#pragma mark - Generate PDF
-(NSString*)render
{
    NSString *fullFilePath = [self.filePath stringByAppendingPathComponent:self.fileName];
    
    BOOL success = UIGraphicsBeginPDFContextToFile(fullFilePath, CGRectZero, nil);
    
    if (success)
    {
        for (RDPDFPage *page in self.pages)
        {
            UIGraphicsBeginPDFPageWithInfo((CGRect){0, 0, page.size.width, page.size.height}, nil);
            
            for (RDPDFElement *element in page.elements)
            {
                [element render];
            }
        }
        
        UIGraphicsEndPDFContext();
    }
    else
    {
        NSLog(@"PDF file creation error!");
        fullFilePath = nil;
    }

    return fullFilePath;
}

@end
