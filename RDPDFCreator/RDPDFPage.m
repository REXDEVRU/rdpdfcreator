//
//  RDPDFPage.m
//
//  Created by Roman Kolesnichenko on 05.03.15.
//  Copyright (c) 2015 Roman Kolesnichenko. All rights reserved.
//

#import "RDPDFPage.h"
#import "RDPDFTextElement.h"
#import "RDPDFImageElement.h"
#import "NSString+Template.h"

@implementation RDPDFPage

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        self.size = DEFAULT_PAGE_SIZE;
    }
    return self;
}

-(instancetype)initWithSize:(CGSize)size
{
    self = [super init];
    if (self)
    {
        self.size = size;
    }
    return self;
}

+(instancetype)pageWithSize:(CGSize)size
{
    return [[RDPDFPage alloc] initWithSize:size];
}

+(instancetype)pageWithNibNamed:(NSString*)nibName values:(NSDictionary*)values
{
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];

    RDPDFPage *page = [RDPDFPage new];

    if (objects && objects.count > 0)
    {
        UIView *mainView = [objects objectAtIndex:0];
        page.size = mainView.frame.size;
        
        for (UIView *object in mainView.subviews)
        {
            if ([object isKindOfClass:[UILabel class]])
            {
                UILabel *label = (UILabel*)object;

                NSString *value = label.text;

                if (value.isTemplateWithParam)
                {
                    [page createGroupElementForLabel:label values:values];
                }
                else
                {
                    [page createSimpleElementForLabel:label values:values];
                }
            }
            else if ([object isKindOfClass:[UIImageView class]])
            {
                UIImageView *imageView = (UIImageView*)object;
                if (imageView.image)
                {
                    UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, NO, 4.0);
                    [imageView.layer renderInContext:UIGraphicsGetCurrentContext()];
                    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
                    UIGraphicsEndImageContext();
                    
                    RDPDFImageElement *imageElement = [RDPDFImageElement image:image];
                    imageElement.frame = imageView.frame;
                    [page addElement:imageElement];
                }
            }
        }
    }
    
    return page;
}

#pragma mark -
-(void) createGroupElementForLabel:(UILabel*)label values:(NSDictionary*)values
{
    NSString *key = label.text;

    NSNumber *numberElements = [key paramValue];
    NSString *templateName = [key templateName];

    NSString *valueString = [values objectForKey:templateName];
    NSArray *symbolsArray = [valueString splitByOneSymbol];
    
    NSInteger index = 0;
    CGRect frame = label.frame;
    
    for (NSString *symbol in symbolsArray)
    {
        RDPDFTextElement *textElement = [RDPDFTextElement text:symbol];
        textElement.frame = frame;
        textElement.textAlignment = label.textAlignment;
        textElement.textColor = label.textColor;
        textElement.lineBreakMode = label.lineBreakMode;
        textElement.font = label.font;
        
        [self addElement:textElement];
        
        index++;
        frame.origin.x = frame.origin.x + frame.size.width;
        
        if (numberElements.integerValue > 0 && numberElements.integerValue < index)
        {
            break;
        }
    }
}

-(void) createSimpleElementForLabel:(UILabel*)label values:(NSDictionary*)values
{
    NSString *value = label.text;
    
    if (value.isTemplate)
    {
        value = [values objectForKey:value];
        
        if (!value)
        {
            value = label.text;
        }
    }
    
    RDPDFTextElement *textElement = [RDPDFTextElement text:value];
    textElement.frame = label.frame;
    textElement.textAlignment = label.textAlignment;
    textElement.textColor = label.textColor;
    textElement.lineBreakMode = label.lineBreakMode;
    textElement.font = label.font;
    
    [self addElement:textElement];
}

#pragma mark -
-(void)addElement:(RDPDFElement *)element
{
    [self.elements addObject:element];
}

-(void)insertElement:(RDPDFElement *)element atIndex:(NSUInteger)index
{
    [self.elements insertObject:element atIndex:index];
}

-(NSUInteger)indexOfElement:(RDPDFElement *)element
{
    return [self.elements indexOfObject:element];
}

#pragma mark -
-(NSMutableArray *)elements
{
    if (!_elements)
    {
        _elements = [NSMutableArray new];
    }
    return _elements;
}

@end
