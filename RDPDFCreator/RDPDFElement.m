//
//  RDPDFElement.m
//
//  Created by Roman Kolesnichenko on 05.03.15.
//  Copyright (c) 2015 Roman Kolesnichenko. All rights reserved.
//

#import "RDPDFElement.h"

@implementation RDPDFElement

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.visible = YES;
    }
    return self;
}

-(void)setFrame:(CGRect)frame
{
    _frame = frame;
    _position = frame.origin;
    _size = frame.size;
    _width = frame.size.width;
    _height = frame.size.height;
}

-(void)setSize:(CGSize)size
{
    _size = size;
    _frame.size = size;
    _width = size.width;
    _height = size.height;
}

-(void)setPosition:(CGPoint)position
{
    _position = position;
    _frame.origin = position;
}

-(void)setWidth:(CGFloat)width
{
    _width = width;
    _size.width = width;
    _frame.size.width = width;
}

-(void)setHeight:(CGFloat)height
{
    _height = height;
    _frame.size.height = height;
    _size.height = height;
}

#pragma mark -
-(void)render
{

}

@end
