//
//  NSString+Template.m
//
//  Created by Roman Kolesnichenko on 05.03.15.
//  Copyright (c) 2015 Roman Kolesnichenko. All rights reserved.
//

#import "NSString+Template.h"

@implementation NSString (Template)

-(BOOL)isTemplate
{
    if (self.length > 1 && [[self substringToIndex:1] isEqualToString:@"#"])
    {
        return YES;
    }
    
    return NO;
}

-(BOOL)isTemplateWithParam
{
    NSString *regex = @"#[A-Z0-9a-z._%+-]+\\[[0-9]+\\]";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    
    return [predicate evaluateWithObject:self];
}

-(NSNumber*)paramValue
{
    NSNumber *result = nil;
    NSError *error = nil;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"#[A-Z0-9a-z._%+-]+\\[([0-9]+)+\\]" options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSArray *matches = [regex matchesInString:self options:0 range:(NSRange){0, self.length}];
    
    if (matches.count > 0)
    {
        NSTextCheckingResult *match = [matches objectAtIndex:0];
        
        NSString *resultString = [self substringWithRange:[match rangeAtIndex:1]];
        result = @(resultString.integerValue);
    }

    return result;
}

-(NSString*)templateName
{
    NSString *result = nil;
    NSError *error = nil;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(#[A-Z0-9a-z._%+-]+)\\[[0-9]+\\]" options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSArray *matches = [regex matchesInString:self options:0 range:(NSRange){0, self.length}];
    
    if (matches.count > 0)
    {
        NSTextCheckingResult *match = [matches objectAtIndex:0];
        result = [self substringWithRange:[match rangeAtIndex:1]];
    }
    
    return result;
}

-(NSArray*)splitByOneSymbol
{
    return [self splitByLength:1];
}

-(NSArray*)splitByLength:(NSUInteger)len
{
    NSMutableArray *groups = [NSMutableArray new];
    NSMutableArray *characters = [NSMutableArray new];
    
    for (int i=0; i < self.length; i++)
    {
        NSString *singleChar  = [NSString stringWithFormat:@"%c", [self characterAtIndex:i]];
        [characters addObject:singleChar];
        
        if (characters.count == len)
        {
            [groups addObject:[characters componentsJoinedByString:@""]];
            [characters removeAllObjects];
        }
    }
    
    if (characters.count > 0)
    {
        for (NSUInteger i = len-characters.count; i<len+1; i++)
        {
            [characters insertObject:@"0" atIndex:0];
        }
        
        [groups addObject:[characters componentsJoinedByString:@""]];
    }
    
    return [NSArray arrayWithArray:groups];
}

@end
