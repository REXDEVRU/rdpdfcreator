//
//  RDPDFTextElement.m
//
//  Created by Roman Kolesnichenko on 05.03.15.
//  Copyright (c) 2015 Roman Kolesnichenko. All rights reserved.
//

#import "RDPDFTextElement.h"

#import <CoreText/CoreText.h>

@interface RDPDFTextElement()
@property (nonatomic, readonly) NSDictionary *stringAttributes;
@end

@implementation RDPDFTextElement

-(instancetype)initWithText:(NSString*)text
{
    self = [super init];
    if (self)
    {
        self.text = text;
        
        self.textAlignment = NSTextAlignmentLeft;
        self.lineBreakMode = NSLineBreakByWordWrapping;
    }
    return self;
}

-(instancetype)initWithText:(NSString*)text width:(CGFloat)width
{
    self = [super init];
    if (self)
    {
        self.width = width;
        self.text = text;

        self.textAlignment = NSTextAlignmentLeft;
        self.lineBreakMode = NSLineBreakByWordWrapping;
    }
    return self;
}

+(instancetype) text:(NSString*)text
{
    return [[RDPDFTextElement alloc] initWithText:text];
}

+(instancetype) text:(NSString*)text width:(CGFloat)width
{
    return [[RDPDFTextElement alloc] initWithText:text width:width];
}

#pragma mark -
-(UIColor *)textColor
{
    if (!_textColor)
    {
        _textColor = DEFAULT_TEXT_COLOR;
    }
    
    return _textColor;
}

-(UIFont *)font
{
    if (!_font)
    {
        _font = DEFAULT_TEXT_FONT;
    }

    return _font;
}

-(NSDictionary*)stringAttributes
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = self.lineBreakMode;
    paragraphStyle.alignment = self.textAlignment;
    
    return  @{
              NSFontAttributeName:self.font,
              NSParagraphStyleAttributeName:paragraphStyle,
              NSForegroundColorAttributeName:self.textColor
               };
}

#pragma mark -
-(void)render
{
    if (self.text && self.text.length>0)
    {
        NSDictionary *stringAttributes = self.stringAttributes;
        
        if (self.width>0)
        {
            CGSize size = [self.text boundingRectWithSize:(CGSize){self.width, CGFLOAT_MAX}
                                                  options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                               attributes:stringAttributes
                                                  context:nil].size;
            if (self.height > 0)
            {
                CGFloat yPos = self.position.y;
                
                if (self.height > size.height)
                {
                    yPos = yPos + (self.height/2 - size.height/2);
                }
                
                CGFloat xPos = self.position.x;
                
                self.position = ccp(xPos, yPos);
                self.height = size.height;
            }
            
        }
        else
        {
            self.size = [self.text sizeWithAttributes:stringAttributes];
        }
        
       // [self.text drawInRect:self.frame withAttributes:stringAttributes];
        [self.text drawWithRect:self.frame options:NSStringDrawingUsesLineFragmentOrigin attributes:stringAttributes context:nil];
        
    }
    
}




@end
