//
//  RDPDFTextElement.h
//
//  Created by Roman Kolesnichenko on 05.03.15.
//  Copyright (c) 2015 Roman Kolesnichenko. All rights reserved.
//

#import "RDPDFElement.h"
#import "RDPDFConstants.h"

@interface RDPDFTextElement : RDPDFElement

-(instancetype)initWithText:(NSString*)text;
-(instancetype)initWithText:(NSString*)text width:(CGFloat)width;

+(instancetype)text:(NSString*)text;
+(instancetype)text:(NSString*)text width:(CGFloat)width;

-(void)render;

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIColor *textColor;

@property (nonatomic, assign) NSTextAlignment textAlignment;
@property (nonatomic, assign) NSLineBreakMode lineBreakMode;

@end
