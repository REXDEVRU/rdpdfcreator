**RDPDFCreator** is a simple iOS library for creation PDF file.

# Installation
To use **RDPDFCreator** in an app, just drag the **RDPDFCreator** folder into your project and import header file.

```objective-c
#import "RDPDFCreator.h"
```

# Usage

```
#!objective-c
    RDPDFCreator *pdfCreator = [RDPDFCreator new];
    
    RDPDFPage *page = [RDPDFPage pageWithSize:PAGE_SIZE_A4];
    
    RDPDFTextElement *textElement = [RDPDFTextElement text:@"Alex"];
    textElement.position = (CGPoint){100, 100};
    textElement.font = [UIFont systemFontOfSize:14];
    textElement.textColor = [UIColor redColor];
    [page addElement:textElement];
    
    RDPDFImageElement *imageElement = [RDPDFImageElement imageNamed:@"photo"];
    imageElement.position = (CGPoint){10, 100};
    [page addElement:imageElement];
    
    [pdfCreator addPage:page];
    
    NSString *filePath = [pdfCreator render];

```


Use **.xib** template.
```
#!objective-c
    NSDictionary *values = @{
                                @"#rINN" : @"0123456789",
                                @"#paymentDescription" : @"some text"
                             };

    RDPDFCreator *pdfCreator = [RDPDFCreator new];    
    // create page object with template in XIB file
    // values is NSDictionary object contains NSString values with @"#paramName" keys
    RDPDFPage *page = [RDPDFPage pageWithNibNamed:@"nameOfXibFile" values:values];
    
    [pdfCreator addPage:page];
    
    NSString *filePath = [pdfCreator render];
```


![xib.png](https://bitbucket.org/repo/xyd4En/images/483206672-xib.png)